#Copyright (c) <2021> <submariner> All rights reserved.
#
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
#THIS SOFTWARE IS PROVIDED "AS IS" AND ANYEXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALLBerkeley Software Design, Inc. BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# assumes you have 0ad stable installed with its deps.
# as testing latest diffs require latest code and some
# things has to be adjusted manually often I find it 
# more convenient not to install it properly, therefore
# not making a package. At the moment works with -current
# $(pwd) agnostic script

#!/usr/bin/bash
ATLAS='disable' # disable/enable
THREADS='9' # cores you have +1
WORDIR=/home/$(/usr/bin/whoami)
APNAME='0ad-dev'

$WORDIR/$APNAME/build/workspaces/clean-workspaces.sh

/usr/bin/svn co https://svn.wildfiregames.com/public/ps/trunk/ $WORDIR/$APNAME

$WORDIR/$APNAME/build/workspaces/update-workspaces.sh \
  --without-pch \
  --without-tests \ 
  --$ATLAS-atlas \
  -j$THREADS
  
/usr/bin/make \
  -j$THREADS \
  -C $WORDIR/$APNAME/build/workspaces/gcc

/usr/bin/svn info $WORDIR/$APNAME
/usr/bin/echo "It took $SECONDS seconds"